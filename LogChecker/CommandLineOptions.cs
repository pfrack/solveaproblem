﻿using CommandLine;

namespace LogChecker
{
    public class CommandLineOptions
    {
        [Option('a', "amount", Required = false, HelpText = "Amount of longest transaction in log.", Default = 5)]
        public int Amount { get; set; }

        [Option('f', "failed", Required = false, HelpText = "Maximum percentage of failed", Default = 0.05)]
        public double Failed { get; set; }

        [Option('d', "duration", Required = false, HelpText = "Duration for Percentil transaction in seconds",
            Default = 2)]
        public double Duration { get; set; }

        [Option('p', "percentil", Required = false, HelpText = "Percentil of transaction", Default = 0.9)]
        public double Percentil { get; set; }

        [Option('l', "log", Required = true, HelpText = "File path.")]
        public string LogFile { get; set; }
    }
}