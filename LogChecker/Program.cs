﻿using System;
using System.IO;
using CommandLine;

namespace LogChecker
{
    internal static class Program
    {
        private static void Main(string[] args)
        {
            Parser.Default.ParseArguments<CommandLineOptions>(args)
                .WithParsed(o =>
                {
                    if (File.Exists(o.LogFile))
                    {
                        try
                        {
                            new Checker.LogChecker(File.ReadLines(o.LogFile), o.Amount, o.Failed, o.Percentil,
                                o.Duration).ShowResult();
                        }
                        catch (Exception e)
                        {
                            Console.WriteLine("There was an error" + e);
                        }
                    }
                    else
                    {
                        Console.WriteLine("File not exist");
                    }
                });
        }
    }
}