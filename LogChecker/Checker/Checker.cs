﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LogChecker.Checker
{
    public class LogChecker
    {
        private ConcurrentDictionary<string, string> Starts = new();
        private ConcurrentDictionary<string, string> Ends = new();
        public ConcurrentDictionary<string, double> Times = new();
        internal ConcurrentDictionary<string, decimal> Amount = new();
        internal ConcurrentDictionary<string, decimal> Failed = new();
        internal ConcurrentDictionary<string, decimal> Centil = new();
        private IEnumerable<string> readLines;
        private int amount;
        private decimal failed;
        private decimal percentil;
        private double duration;

        public LogChecker(IEnumerable<string> readLines, int amount, double failed, double percentil, double duration)
        {
            this.readLines = readLines;
            this.amount = amount;
            this.failed = new decimal(failed);
            this.percentil = new decimal(percentil);
            this.duration = duration * 1000;
        }

        private const string DateFormat = "yyyy-MM-dd HH:mm:ss,fff";

        internal void GatherData()
        {
            Parallel.ForEach(readLines, (line, _, _) =>
            {
                var strings = line.Split(" ");
                var timestamp = strings[0] + " " + strings[1];
                var action = strings[3];
                var uuid = strings[4];
                var phase = strings[5];
                if (phase.Contains("Start"))
                {
                    Starts[uuid + action] = timestamp;
                    Amount.AddOrUpdate(action, _ => 1, (_, exist) => exist + 1);
                }
                else
                {
                    Ends[uuid + action] = timestamp;
                    if (phase.Contains("Failed"))
                        Failed.AddOrUpdate(action, _ => 1, (_, exist) => exist + 1);
                }
            });
            Parallel.ForEach(Starts, kv => Calculate(kv.Key.Split("]:")[1], kv.Value, Ends[kv.Key]));
        }

        private void Calculate(string action, string start, string end)
        {
            var length = GetLength(start, end);
            if (length >= duration)
            {
                Centil.AddOrUpdate(action, _ => 1, (_, exist) => exist + 1);
            }

            Times.AddOrUpdate(action, _ => length, (_, exist) => exist + length);
        }


        internal IEnumerable<KeyValuePair<string, double>> Longest()
        {
            return Times.OrderByDescending(entry => entry.Value).Take(amount);
        }

        private static double GetLength(string start, string end)
        {
            return (DateTime.ParseExact(end, DateFormat, CultureInfo.InvariantCulture) -
                    DateTime.ParseExact(start, DateFormat, CultureInfo.InvariantCulture)).TotalMilliseconds;
        }

        public void ShowResult()
        {
            GatherData();
            Console.Write(CheckPercentil());

            Console.Write(CheckFailed());

            foreach (var (key, value) in Longest())
            {
                Console.WriteLine("{0} {1}", value, key);
            }
        }

        internal string CheckPercentil()
        {
            var str = new StringBuilder();
            foreach (var (key, value) in Centil)
            {
                if (decimal.Divide(value, Amount[key]) >= 1 - percentil)
                {
                    str.AppendFormat("{0} Percentil for Action for {1} was bigger than {2} seconds\n", percentil, key,
                        duration / 1000);
                }
            }

            return str.ToString();
        }

        internal string CheckFailed()
        {
            var str = new StringBuilder();
            foreach (var (key, value) in Failed)
            {
                if (value / Amount[key] >= failed)
                {
                    str.AppendFormat("{0}/{1} for action {2} is bigger than {3} percent\n", value,
                        Amount[key], key, failed);
                }
            }

            return str.ToString();
        }
    }
}