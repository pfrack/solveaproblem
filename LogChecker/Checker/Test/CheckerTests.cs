using System.Collections.Generic;
using System.Linq;
using NUnit.Framework;

namespace LogChecker.Checker.Test
{
    public class CheckerTests
    {
        private const double Failed = 0.05;
        private const int Amount = 5;
        private const double Percentil = 0.9;
        private const int Duration = 2;

        [Test]
        public void TestAmount()
        {
            const int amount = 20;
            var readLines = new List<string>();
            for (var i = 0; i < amount; i++)
            {
                readLines.Add($"2021-06-28 13:52:38,606 INFO: [Amount{i}] [{i}]: Start");
                readLines.Add($"2021-06-28 13:52:39,606 INFO: [Amount{i}] [{i}]: End");
            }

            var lc = new LogChecker(readLines, Amount, Failed, Percentil, Duration);
            lc.GatherData();
            Assert.AreEqual(amount, lc.Amount.Count);
            Assert.AreEqual(amount, lc.Times.Count);
            Assert.AreEqual(0, lc.Failed.Count);
            Assert.AreEqual(0, lc.Centil.Count);
        }

        [Test]
        public void TestLongest()
        {
            const int forCount = 20;
            const int amount = 10;
            var readLines = new List<string>();
            for (var j = 0; j < forCount; j++)
            {
                for (var i = amount; i > 0; i--)
                {
                    readLines.Add($"2021-06-28 13:52:{38},606 INFO: [Biggest{i}] [{j}20{i}]: Start");
                    readLines.Add($"2021-06-28 13:52:{38 + i},606 INFO: [Biggest{i}] [{j}20{i}]: End");
                }
            }

            var lc = new LogChecker(readLines, Amount, Failed, Percentil, Duration);
            lc.GatherData();
            var longest = lc.Longest();
            Assert.AreEqual(5, longest.Count());
            var counter = 10;
            foreach (var (key, value) in longest)
            {
                Assert.AreEqual($"[Biggest{counter}]", key);
                Assert.AreEqual(counter-- * forCount * 1000, value);
            }
        }

        [Test]
        public void TestTime()
        {
            var readLines = new List<string>();
            const int oneSecondNumber = 20;
            const int twoSecondsNumber = 10;
            for (var i = 0; i < oneSecondNumber; i++)
            {
                readLines.Add($"2021-06-28 13:52:38,606 INFO: [TimeCheck] [{i}]: Start");
                readLines.Add($"2021-06-28 13:52:39,606 INFO: [TimeCheck] [{i}]: End");
            }

            for (var i = oneSecondNumber; i < oneSecondNumber + twoSecondsNumber; i++)
            {
                readLines.Add($"2021-06-28 13:52:38,606 INFO: [TimeCheck] [{i}]: Start");
                readLines.Add($"2021-06-28 13:52:40,606 INFO: [TimeCheck] [{i}]: End");
            }

            var lc = new LogChecker(readLines, Amount, Failed, Percentil, Duration);
            lc.GatherData();
            Assert.AreEqual(1000 * oneSecondNumber + 2000 * twoSecondsNumber, lc.Times["[TimeCheck]"]);
            Assert.AreEqual(twoSecondsNumber, lc.Centil["[TimeCheck]"]);
        }


        [Test]
        public void TestFailed()
        {
            var readLines = new List<string>();
            for (var i = 0; i < 95; i++)
            {
                readLines.Add($"2021-06-28 13:52:38,606 INFO: [Failed] [{i}]: Start");
                readLines.Add($"2021-06-28 13:52:38,909 INFO: [Failed] [{i}]: End");
                readLines.Add($"2021-06-28 13:52:38,606 INFO: [NoFailed] [20{i}]: Start");
                readLines.Add($"2021-06-28 13:52:38,909 INFO: [NoFailed] [20{i}]: End");
            }

            for (var i = 95; i < 100; i++)
            {
                readLines.Add($"2021-06-28 13:52:38,606 INFO: [Failed] [{i}]: Start");
                readLines.Add($"2021-06-28 13:52:38,909 INFO: [Failed] [{i}]: Failed");
                readLines.Add($"2021-06-28 13:52:38,606 INFO: [NoFailed] [20{i}]: Start");
                readLines.Add($"2021-06-28 13:52:38,909 INFO: [NoFailed] [20{i}]: End");
            }

            var lc = new LogChecker(readLines, Amount, Failed, Percentil, Duration);
            lc.GatherData();
            var checkFailed = lc.CheckFailed().Trim('\n');
            Assert.AreEqual(1, checkFailed.Split("\n").Length);
        }

        [Test]
        public void TestCentil()
        {
            var readLines = new List<string>();
            var i = 0;
            for (; i < 89; i++)
            {
                readLines.Add($"2021-06-28 13:52:38,606 INFO: [Centil] [{i}]: Start");
                readLines.Add($"2021-06-28 13:52:38,909 INFO: [Centil] [{i}]: End");
                readLines.Add($"2021-06-28 13:52:38,606 INFO: [NoCentil] [20{i}]: Start");
                readLines.Add($"2021-06-28 13:52:38,909 INFO: [NoCentil] [20{i}]: End");
            }

            i++;
            readLines.Add($"2021-06-28 13:52:38,606 INFO: [Centil] [{i}]: Start");
            readLines.Add($"2021-06-28 13:52:40,606 INFO: [Centil] [{i}]: End");
            for (; i < 100; i++)
            {
                readLines.Add($"2021-06-28 13:52:38,606 INFO: [Centil] [{i}]: Start");
                readLines.Add($"2021-06-28 13:52:40,606 INFO: [Centil] [{i}]: End");
                readLines.Add($"2021-06-28 13:52:38,606 INFO: [NoCentil] [20{i}]: Start");
                readLines.Add($"2021-06-28 13:52:40,606 INFO: [NoCentil] [20{i}]: End");
            }

            var lc = new LogChecker(readLines
                , Amount, Failed, Percentil, Duration);
            lc.GatherData();
            var checkCentil = lc.CheckPercentil().Trim('\n');
            Assert.AreEqual(1, checkCentil.Split("\n").Length);
        }
    }
}