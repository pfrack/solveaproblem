# Description

LogChecker 1.0.0
Copyright (C) 2021 LogChecker

  -a, --amount       (Default: 5) Amount of longest transaction in log.

  -f, --failed       (Default: 0,05) Maximum percentage of failed

  -d, --duration     (Default: 2) Duration for Percentil transaction in seconds

  -p, --percentil    (Default: 0,9) Percentil of transaction

  -l, --log          Required. File path.

  --help             Display this help screen.

  --version          Display version information.
